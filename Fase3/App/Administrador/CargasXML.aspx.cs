﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Xml;

namespace Fase3.App.Administrador
{
    public partial class CargasXML : System.Web.UI.Page
    {
        Conexion conexion = new Conexion();
        static string id, nombres;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
            }
        }

        //Conversion de fechas

        public string dateToYMD(string fecha)
        {
            String[] partes = fecha.Split('/');
            string dia = partes[0].Trim();
            string mes = partes[1].Trim();
            string anio = partes[2].Trim();
            string fechaSQL = anio + "/" + mes + "/" + dia;
            return fechaSQL;
        }

        public string toDMY(string fecha)
        {
            String[] partes = fecha.Split('/');
            string anio = partes[0].Trim();
            string mes = partes[1].Trim();
            string dia = partes[2].Trim();
            string fechaVista = dia + "/" + mes + "/" + anio;
            return fechaVista;
        }

        //Carga de Puestos

        protected void btnPuestos_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLPuestos(path);
        }

        public void cargaXMLPuestos(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Puesto = ((XmlElement)nodoDefinicion).GetElementsByTagName("puesto");
                    foreach (XmlElement nodoPuesto in Puesto)
                    {
                        XmlNodeList puestoID = nodoPuesto.GetElementsByTagName("codigo");
                        XmlNodeList nombre = nodoPuesto.GetElementsByTagName("nombre");

                        
                        string query = "INSERT INTO Puesto VALUES (" + puestoID[0].InnerText.Trim() + ", '" + nombre[0].InnerText.Trim() + "')";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga de Empleados
        protected void btnEmpleados_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLEmpleados(path);
        }

        public void cargaXMLEmpleados(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Empleado = ((XmlElement)nodoDefinicion).GetElementsByTagName("empleado");
                    foreach (XmlElement nodoCliente in Empleado)
                    {
                        XmlNodeList empleadoNIT = nodoCliente.GetElementsByTagName("NIT");
                        XmlNodeList nombres = nodoCliente.GetElementsByTagName("nombres");
                        XmlNodeList apellidos = nodoCliente.GetElementsByTagName("apellidos");
                        XmlNodeList nacimiento = nodoCliente.GetElementsByTagName("nacimiento");
                        XmlNodeList direccion = nodoCliente.GetElementsByTagName("direccion");
                        XmlNodeList telefono = nodoCliente.GetElementsByTagName("telefono");
                        XmlNodeList celular = nodoCliente.GetElementsByTagName("celular");
                        XmlNodeList email = nodoCliente.GetElementsByTagName("email");
                        XmlNodeList puesto = nodoCliente.GetElementsByTagName("codigo_puesto");
                        XmlNodeList jefe = nodoCliente.GetElementsByTagName("codigo_jefe");
                        XmlNodeList password = nodoCliente.GetElementsByTagName("pass");

                        string prueba = "'"+empleadoNIT[0].InnerText.Trim()+"'";


                        string fecha = nacimiento[0].InnerText;
                        string fechaSQL = dateToYMD(fecha);

                        if (jefe[0].InnerText == "")
                        {
                            string query = "INSERT INTO Empleado(NITempleado, nombres, apellidos, fechaNacimiento, direccion, telefono, celular," +
                             " email, puesto, passwo) " +
                             "VALUES('" + empleadoNIT[0].InnerText.Trim() + "', '" + nombres[0].InnerText.Trim() + "', '" + apellidos[0].InnerText.Trim() + "'," +
                             " '" + fechaSQL + "', '" + direccion[0].InnerText.Trim() + "', '" + telefono[0].InnerText.Trim() + "', " +
                             "'" + celular[0].InnerText.Trim() + "', '" + email[0].InnerText.Trim() + "', " + puesto[0].InnerText.Trim() +
                             ", '" + password[0].InnerText.Trim() + "')";

                                conexion.sqlAbrirConexion();
                                conexion.sqlInsert(query);
                                conexion.sqlCerrarConexion();

                            }
                        else
                        {
                            string query = "INSERT INTO Empleado(NITempleado, nombres, apellidos, fechaNacimiento, direccion, telefono, celular," +
                             " email, puesto, jefe, passwo) " +
                             "VALUES('" + empleadoNIT[0].InnerText.Trim() + "', '" + nombres[0].InnerText.Trim() + "', '" + apellidos[0].InnerText.Trim() + "'," +
                             " '" + fechaSQL + "', '" + direccion[0].InnerText.Trim() + "', '" + telefono[0].InnerText.Trim() + "', " +
                             "'" + celular[0].InnerText.Trim() + "', '" + email[0].InnerText.Trim() + "', " + puesto[0].InnerText.Trim() +
                             ", '" + jefe[0].InnerText.Trim() + "'," + " '" + password[0].InnerText.Trim() + "')";

                                conexion.sqlAbrirConexion();
                                conexion.sqlInsert(query);
                                conexion.sqlCerrarConexion();
                        }
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga de Categorias

        protected void btnCategorias_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLCategorias(path);
        }

        public void cargaXMLCategorias(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Categoria = ((XmlElement)nodoDefinicion).GetElementsByTagName("categoria");
                    foreach (XmlElement nodoCategoria in Categoria)
                    {
                        XmlNodeList categoriaID = nodoCategoria.GetElementsByTagName("codigo");
                        XmlNodeList nombre = nodoCategoria.GetElementsByTagName("nombre");


                        string query = "INSERT INTO Categoria (idCategoria, categoria) VALUES (" + categoriaID[0].InnerText.Trim()
                            + ", '" + nombre[0].InnerText.Trim() + "');";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga de Productos

        protected void btnProductos_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLProductos(path);
        }

        public void cargaXMLProductos(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Producto = ((XmlElement)nodoDefinicion).GetElementsByTagName("producto");
                    foreach (XmlElement nodoProducto in Producto)
                    {
                        XmlNodeList productoID = nodoProducto.GetElementsByTagName("codigo");
                        XmlNodeList nombre = nodoProducto.GetElementsByTagName("nombre");
                        XmlNodeList categoria = nodoProducto.GetElementsByTagName("categoria");


                        string query = "INSERT INTO Producto (idProducto, nombre, categoria) VALUES " +
                            "(" + productoID[0].InnerText.Trim() + ", '" + nombre[0].InnerText.Trim() + "'," + categoria[0].InnerText.Trim() + ")";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga de Departamentos

        protected void btnDepto_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLDepartamentos(path);
        }

        public void cargaXMLDepartamentos(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Departamento = ((XmlElement)nodoDefinicion).GetElementsByTagName("depto");
                    foreach (XmlElement nodoDepartamento in Departamento)
                    {
                        XmlNodeList departamentoID = nodoDepartamento.GetElementsByTagName("codigo");
                        XmlNodeList nombreDep = nodoDepartamento.GetElementsByTagName("nombre");

                        string query = "INSERT INTO Departamento(idDepartamento, departamento) VALUES (" + departamentoID[0].InnerText.Trim() +
                            ", '" + nombreDep[0].InnerText.Trim() + "');";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();
                    }
                }

                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga de Municipios

        protected void btnMuni_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLMunicipios(path);
        }

        public void cargaXMLMunicipios(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Municipio = ((XmlElement)nodoDefinicion).GetElementsByTagName("ciudad");
                    foreach (XmlElement nodoMunicipio in Municipio)
                    {
                        XmlNodeList municipioID = nodoMunicipio.GetElementsByTagName("codigo");
                        XmlNodeList nombreMuni = nodoMunicipio.GetElementsByTagName("nombre");
                        XmlNodeList cod_departamento = nodoMunicipio.GetElementsByTagName("codigo_depto");


                        string query = "INSERT INTO Municipio(idMunicipio, municipio, departamento) VALUES(" + municipioID[0].InnerText.Trim()
                            + ", '" + nombreMuni[0].InnerText.Trim() + "', " + cod_departamento[0].InnerText.Trim() + ")";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga Monedas

        protected void btnMoneda_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLMonedas(path);
        }

        public void cargaXMLMonedas(string path)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Moneda = ((XmlElement)nodoDefinicion).GetElementsByTagName("moneda");
                    foreach (XmlElement nodoMoneda in Moneda)
                    {
                        XmlNodeList nombre = nodoMoneda.GetElementsByTagName("nombre");
                        XmlNodeList simbolo = nodoMoneda.GetElementsByTagName("simbolo");
                        XmlNodeList tasa = nodoMoneda.GetElementsByTagName("tasa");

                        string query = "INSERT INTO Moneda VALUES('"+ nombre[0].InnerText.Trim() + "', '"+ simbolo[0].InnerText.Trim() +
                            "', "+ tasa[0].InnerText.Trim() + ")";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();
                    }
                }

                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga Cientes

        protected void btnClientes_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLClientes(path);
        }

        public void cargaXMLClientes(string path)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Cliente = ((XmlElement)nodoDefinicion).GetElementsByTagName("cliente");
                    foreach (XmlElement nodoCliente in Cliente)
                    {
                        XmlNodeList clienteNIT = nodoCliente.GetElementsByTagName("NIT");
                        XmlNodeList nombres = nodoCliente.GetElementsByTagName("nombres");
                        XmlNodeList apellidos = nodoCliente.GetElementsByTagName("apellidos");
                        XmlNodeList nacimiento = nodoCliente.GetElementsByTagName("nacimiento");
                        XmlNodeList direccion = nodoCliente.GetElementsByTagName("direccion");
                        XmlNodeList telefono = nodoCliente.GetElementsByTagName("telefono");
                        XmlNodeList celular = nodoCliente.GetElementsByTagName("celular");
                        XmlNodeList email = nodoCliente.GetElementsByTagName("email");
                        XmlNodeList municipio = nodoCliente.GetElementsByTagName("ciudad");
                        XmlNodeList departamento = nodoCliente.GetElementsByTagName("depto");
                        XmlNodeList limitCr = nodoCliente.GetElementsByTagName("limite_credito");
                        XmlNodeList diasCr = nodoCliente.GetElementsByTagName("días_credito");


                        string fechaNac = dateToYMD(nacimiento[0].InnerText);

                        string query = "INSERT INTO Cliente VALUES ('"+ clienteNIT[0].InnerText.Trim() + "', '"+ nombres[0].InnerText.Trim() + "'," +
                            " '"+ apellidos[0].InnerText.Trim() + "', '"+ fechaNac +"', '"+ direccion[0].InnerText.Trim() + "', " +
                            "'"+ telefono[0].InnerText.Trim() + "', '"+ celular[0].InnerText.Trim() + "', '"+ email[0].InnerText.Trim() + 
                            "', "+ municipio[0].InnerText.Trim() + ", " + ""+ departamento[0].InnerText.Trim() + ", "+limitCr[0] .InnerText.Trim() +
                            ", "+ diasCr[0].InnerText.Trim() + ")";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();

                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga Listas

        protected void btnListas_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLListas(path);
        }

        public void cargaXMLListas(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Lista = ((XmlElement)nodoDefinicion).GetElementsByTagName("lista");
                    foreach (XmlElement nodoLista in Lista)
                    {
                        XmlNodeList listaID = nodoLista.GetElementsByTagName("codigo");
                        XmlNodeList nombreLista = nodoLista.GetElementsByTagName("nombre");
                        XmlNodeList fechaIni = nodoLista.GetElementsByTagName("vigencia_inicio");
                        XmlNodeList fechaFin = nodoLista.GetElementsByTagName("vigencia_final");

                        string initD = dateToYMD(fechaIni[0].InnerText);
                        string finD = dateToYMD(fechaFin[0].InnerText);

                        string query = "INSERT INTO Lista (idLista, nombre, fechaInicio, fechaFin)" +
                            " VALUES (" + listaID[0].InnerText.Trim() + ", '" + nombreLista[0].InnerText.Trim() + "', '" + initD + "', '" + finD + "')";
                        ;

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();

                        XmlNodeList Detalle = ((XmlElement)nodoLista).GetElementsByTagName("detalle");
                        foreach (XmlElement nodoDetalle in Detalle)
                        {
                            XmlNodeList Item = ((XmlElement)nodoDetalle).GetElementsByTagName("item");
                            foreach (XmlElement nodoItem in Item)
                            {
                                XmlNodeList codigoProd = nodoItem.GetElementsByTagName("codigo_producto");
                                XmlNodeList precio = nodoItem.GetElementsByTagName("valor");


                                string query2 = "INSERT INTO ListaProductos(lista, producto, precio)" +
                                " VALUES (" + listaID[0].InnerText.Trim() + ", " + codigoProd[0].InnerText.Trim() + ", " + precio[0].InnerText.Trim() + ")";

                                conexion.sqlAbrirConexion();
                                conexion.sqlInsert(query2);
                                conexion.sqlCerrarConexion();

                            }
                        }


                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga Lista-Productos

        protected void btnLC_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLlc(path);
        }

        public void cargaXMLlc(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList lisCli = ((XmlElement)nodoDefinicion).GetElementsByTagName("lstXCliente");
                    foreach (XmlElement nodoLisCli in lisCli)
                    {
                        XmlNodeList cliente = nodoLisCli.GetElementsByTagName("cliente");
                        XmlNodeList lista = nodoLisCli.GetElementsByTagName("codigo_lista");


                        string query = "INSERT INTO ListaCliente VALUES ('"+ cliente[0].InnerText.Trim() + "', "+ lista[0].InnerText.Trim() + ")";

                        conexion.sqlAbrirConexion();
                        conexion.sqlInsert(query);
                        conexion.sqlCerrarConexion();

                    }
                }
            MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Carga Metas

        protected void btnMetas_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.fileU.FileName);
            cargaXMLMetas(path);
        }

        public void cargaXMLMetas(string path)
        {

            try
            { 
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Meta = ((XmlElement)nodoDefinicion).GetElementsByTagName("meta");
                    foreach (XmlElement nodoMeta in Meta)
                    {
                        XmlNodeList empleadoID = nodoMeta.GetElementsByTagName("NIT_empleado");
                        XmlNodeList mes = nodoMeta.GetElementsByTagName("mes_meta");

                        String[] partes = mes[0].InnerText.Split('/');
                        string mesP = partes[0].Trim();
                        string anio = partes[1].Trim();
                        string fechaSQL = anio + "/" + mesP + "/01";


                        XmlNodeList Detalle = ((XmlElement)nodoMeta).GetElementsByTagName("detalle");
                        foreach (XmlElement nodoDetalle in Detalle)
                        {

                            XmlNodeList Item = ((XmlElement)nodoDetalle).GetElementsByTagName("item");
                            foreach (XmlElement nodoItem in Item)
                            {
                                XmlNodeList producto = nodoItem.GetElementsByTagName("codigo_producto");
                                XmlNodeList cantidad = nodoItem.GetElementsByTagName("meta_venta");

                                string query = "INSERT INTO Meta VALUES ('" + empleadoID[0].InnerText.Trim() + "', '" + fechaSQL + "'," +
                                    " " + producto[0].InnerText.Trim() + ", " + cantidad[0].InnerText.Trim() + ")";

                                conexion.sqlAbrirConexion();
                                conexion.sqlInsert(query);
                                conexion.sqlCerrarConexion();
                            }
                        }


                    }
                }
            MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}