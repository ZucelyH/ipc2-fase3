﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fase3.App.Administrador
{
    public partial class InicioAdmin : System.Web.UI.Page
    {

        static string id, nombres;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();

                lblNUsser.Text = nombres;
            }
        }
    }
}