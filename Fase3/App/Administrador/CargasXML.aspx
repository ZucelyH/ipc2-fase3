﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Administrador/Administrador.Master" AutoEventWireup="true" CodeBehind="CargasXML.aspx.cs" Inherits="Fase3.App.Administrador.CargasXML" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br /><br />
    
    <h1 style="text-align: center;">Cargas XML</h1><br />

    <h5 style="text-align: center;">Escoja el archivo que desea cargar</h5>
    <div style="margin-left: auto; margin-right: auto; text-align: center;">

        <asp:FileUpload ID="fileU" runat="server"></asp:FileUpload><br /><br /><br />

        <asp:Button Type="button" Class="btn btn-info" ID="btnPuestos" runat="server" Text="Cargar Puestos" OnClick="btnPuestos_Click" /> &nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnEmpleados" runat="server" Text="Cargar Empleados" OnClick="btnEmpleados_Click" /> &nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnCategorias" runat="server" Text="Cargar Categorias" OnClick="btnCategorias_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnProductos" runat="server" Text="Cargar Productos" OnClick="btnProductos_Click" /><br /><br />
        <asp:Button Type="button" Class="btn btn-info" ID="btnDepto" runat="server" Text="Cargar Departamentos" OnClick="btnDepto_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnMuni" runat="server" Text="Cargar Municipios" OnClick="btnMuni_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnMoneda" runat="server" Text="Cargar Moneda" OnClick="btnMoneda_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnClientes" runat="server" Text="Cargar Clientes" OnClick="btnClientes_Click" /><br /><br />
        <asp:Button Type="button" Class="btn btn-info" ID="btnListas" runat="server" Text="Cargar Lista" OnClick="btnListas_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnLC" runat="server" Text="Cargar Listas-Clientes" OnClick="btnLC_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button Type="button" Class="btn btn-info" ID="btnMetas" runat="server" Text="Cargar Metas" OnClick="btnMetas_Click" />&nbsp;&nbsp;&nbsp;&nbsp; 

        <!---
            1.Puestos
            2.Empleados
            3.Categorias
            4.Productos
            5.Departamentos
            6.Municipios
            7.Monedas
            8.Clientes
            9.Listas
            10.LC
            11.Metas
            --->

    </div>

</asp:Content>
