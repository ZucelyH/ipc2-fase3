﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fase3.App.Administrador
{
    public partial class Administrador : System.Web.UI.MasterPage
    {
        static string id, nombres;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Login/Login.aspx");
        }

        protected void btnEmpleados_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Administrador/InicioAdmin.aspx?ID=" + id + "&nombres=" + nombres);

        }

        protected void btnCargas_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Administrador/CargasXML.aspx?ID=" + id + "&nombres=" + nombres);

        }


    }
}