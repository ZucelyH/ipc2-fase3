﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Fase3.App.Clases
{
    public class Conexion
    {

        public string conexionSQL = @"Data Source=LEVI; Initial Catalog=Fase3; Integrated Security=True;";
        SqlConnection conexion;

        public void sqlInsert(string query)
        {
            SqlCommand comando = new SqlCommand(query, conexion);
            comando.ExecuteNonQuery();
        }

        public void sqlAbrirConexion()
        {
            conexion = new SqlConnection(conexionSQL);
            conexion.Open();
        }

        public void sqlCerrarConexion()
        {
            conexion.Close();
        }

        public DataTable sqlSelect(string querySelect)
        {
            SqlDataAdapter sqlDA = new SqlDataAdapter(querySelect, conexion);
            DataTable dtbl = new DataTable();
            sqlDA.Fill(dtbl);
            return dtbl;
        }

        public void sqlUpdate(string query)
        {
            SqlCommand comando = new SqlCommand(query, conexion);
            comando.ExecuteNonQuery();
        }

    }
}