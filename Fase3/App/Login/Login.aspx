﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Fase3.App.Login.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" href="~/Content/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="~/Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="~/Scripts/jquery-3.4.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form-group">


            <div style="margin-left: auto; margin-right: auto; text-align: center;">

                <p>
                    &nbsp;</p>
                <p>
                <h1 Font-Bold="true" Font-Size="X-Large" CssClass="StrongText" >Login</h1>
                <p  >&nbsp;</p>


                <p>
                <asp:Label display="inline-block"  align="center" ID="Label1" runat="server" Text="Ingrese su usuario:   "></asp:Label>
                </p> 
                <p>
                <asp:TextBox align="center" ID="txtUsuario" runat="server"></asp:TextBox>
                </p>
                <p>
                    &nbsp;</p>
                <p>
                <asp:Label ID="Label2" runat="server" Text="Ingrese su contraseña:   "></asp:Label>
                </p>
                <p>
                <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                </p>
                <p>
                    &nbsp;</p>
                <asp:Button  type="button" class="btn btn-outline-danger btn-lg" ID="Button1" runat="server" Text="Entrar" OnClick="Button1_Click" />

            </div>


        </div>
    </form>
</body>
</html>
