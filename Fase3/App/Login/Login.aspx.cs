﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace Fase3.App.Login
{
    public partial class Login : System.Web.UI.Page
    {

        Conexion conexion = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtb = new DataTable();

                string query = "SELECT puesto, nombres FROM Empleado WHERE NITempleado = '" + txtUsuario.Text + "' AND passwo = '" + txtPassword.Text + "'";

                conexion.sqlAbrirConexion();
                dtb = conexion.sqlSelect(query);
                conexion.sqlCerrarConexion();

                if (dtb.Rows[0][0].ToString() == "1")
                {
                    MessageBox.Show("Bienvenido " + dtb.Rows[0][1].ToString(), "Ingreso al sistema",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                    Response.Redirect("../Vendedor/InicioVendedor.aspx?ID=" + txtUsuario.Text + "&nombres=" + dtb.Rows[0][1].ToString());
                }
                else if (dtb.Rows[0][0].ToString() == "2")
                {
                    MessageBox.Show("Bienvenido " + dtb.Rows[0][1].ToString(), "Ingreso al sistema",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                    Response.Redirect("../Supervisor/InicioSupervisor.aspx?ID=" + txtUsuario.Text + "&nombres=" + dtb.Rows[0][1].ToString());
                }
                else if (dtb.Rows[0][0].ToString() == "3")
                {
                    MessageBox.Show("Bienvenido " + dtb.Rows[0][1].ToString(), "Ingreso al sistema",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                    Response.Redirect("../Gerente/InicioGerente.aspx?ID=" + txtUsuario.Text + "&nombres=" + dtb.Rows[0][1].ToString());
                }
                else if (dtb.Rows[0][0].ToString() == "4" || dtb.Rows[0][0].ToString() == "21")
                {
                    MessageBox.Show("Bienvenido " + dtb.Rows[0][1].ToString(), "Ingreso al sistema",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                    Response.Redirect("../Administrador/InicioAdmin.aspx?ID=" + txtUsuario.Text + "&nombres=" + dtb.Rows[0][1].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Su usuario no está registrado", "Error al entrar", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            

        }
    }
}