﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fase3.App.Vendedor
{
    public partial class OrdenesGeneradas : System.Web.UI.Page
    {
        static string id, nombres;
        Conexion conexion = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
                mostrarOrdenes();
            }
        }

        public void mostrarOrdenes()
        {
            DataTable ordenes = new DataTable();
            string query = "SELECT idOrden FROM Orden WHERE empleado = '" + id + "' GROUP BY idOrden";
            conexion.sqlAbrirConexion();
            ordenes = conexion.sqlSelect(query);
            conexion.sqlCerrarConexion();

            for (int i = 0; i < ordenes.Rows.Count; i++)
            {
                dropDordenes.Items.Add(ordenes.Rows[i][0].ToString());
            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            mostrarEnGV();
        }

        public void mostrarEnGV()
        {
            DataTable ordenes = new DataTable();
            string query = "SELECT * FROM Orden as O, DetalleOrden as DO WHERE O.idOrden = DO.orden AND idOrden = "+ dropDordenes.SelectedItem.Text +"";
            conexion.sqlAbrirConexion();
            ordenes = conexion.sqlSelect(query);
            conexion.sqlCerrarConexion();

            gvOG.DataSource = ordenes;
            gvOG.DataBind();


        }
    }
}