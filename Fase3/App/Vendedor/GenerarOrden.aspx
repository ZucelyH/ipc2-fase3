﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Vendedor/Vendedor.Master" AutoEventWireup="true" CodeBehind="GenerarOrden.aspx.cs" Inherits="Fase3.App.Vendedor.GenerarOrden" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br /><br />
    <h2 style="text-align: left;">Productos lista vigente</h2><br />
    <asp:GridView ID="gridProductos" runat="server"></asp:GridView>
    <br /><br />

    <h3 style="text-align: center;">Generar Orden</h3><br />

    <div style="margin-left: auto; margin-right: auto; text-align: center;">

        <label for="espec">Seleccione el cliente</label><br />


        <asp:DropDownList  ID="DropDownClientes" runat="server">
            <asp:ListItem Text="-- Seleccione un cliente --"></asp:ListItem>

        </asp:DropDownList>
        <!--&nbsp;&nbsp;<asp:Button  type="button" class="btn btn-info" ID="btnVerifica" runat="server" Text="Verificar listas" />-->
        <br />

    </div>

    <br />

    <label for="espec">Seleccione un producto</label>
    
    <br />


        <asp:DropDownList  ID="dropDPro" runat="server">
            <asp:ListItem Text="-- Seleccione un producto --"></asp:ListItem>

        </asp:DropDownList> 

    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Total: "></asp:Label>
        <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:Label ID="Label2" runat="server" Text="Limite Credito: "></asp:Label>
        <asp:Label ID="lblLimiteCr" runat="server" Text=""></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button type="button" class="btn btn-warning" ID="btnGenerarO" runat="server" Text="Generar Orden" OnClick="btnGenerarO_Click" />
&nbsp;
        <br />
        <asp:Label ID="Label3" runat="server" Text="Ordenes vencidas: "></asp:Label>
        <asp:Label ID="lblOV" runat="server" Text=""></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    
        
    
        <br /><br />
        <label for="espec">Escriba la cantidad</label><br />
        <asp:TextBox ID="txtCantidad" runat="server"></asp:TextBox>
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button type="button" class="btn btn-warning" ID="BbtnAgregar" runat="server" OnClick="btnAgregar_Click" Text="Agregar" />

        <br /><br />


    <asp:GridView ID="gvProAgre" runat="server"></asp:GridView>


</asp:Content>
