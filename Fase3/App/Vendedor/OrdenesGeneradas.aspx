﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Vendedor/Vendedor.Master" AutoEventWireup="true" CodeBehind="OrdenesGeneradas.aspx.cs" Inherits="Fase3.App.Vendedor.OrdenesGeneradas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1 style="text-align: center;">Ordenes Generadas</h1><br />

    <div style="margin-left: auto; margin-right: auto; text-align: center;">

        <label for="espec">Seleccione un id de Orden</label><br />


        <asp:DropDownList  ID="dropDordenes" runat="server">
            <asp:ListItem Text="-- Seleccione una orden --"></asp:ListItem>
        </asp:DropDownList><br /><br />

        <asp:Button  type="button" class="btn btn-success" ID="btnMostrar" runat="server" Text="Ver orden" OnClick="btnMostrar_Click" />

        <br /><br />

    </div>

    <asp:GridView ID="gvOG" runat="server"></asp:GridView>


</asp:Content>
