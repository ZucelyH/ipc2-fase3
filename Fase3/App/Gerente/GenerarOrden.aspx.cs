﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace Fase3.App.Gerente
{
    public partial class GenerarOrden : System.Web.UI.Page
    {
        static string id, nombres, idListaVigente;
        Conexion conexion = new Conexion();
        static int idIngresos = 0;
        static int idOrden;
        static float total = 0, limite;
        static DataTable prod = new DataTable();
        static DataTable lista = new DataTable();
        static DataTable listado = new DataTable();
        static string idCliente;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
                mostrarProductosGrid();
                mostrarClientes();
                mostrarProductos();
                crearDTbl();
                obOr();
            }
        }



        public void mostrarClientes()
        {
            DataTable cli = new DataTable();
            conexion.sqlAbrirConexion();
            cli = conexion.sqlSelect("SELECT nombres FROM Cliente");
            conexion.sqlCerrarConexion();

            for (int i = 0; i < cli.Rows.Count; i++)
            {
                DropDownClientes.Items.Add(cli.Rows[i][0].ToString());
            }

        }

        public void mostrarProductosGrid()
        {
            conexion.sqlAbrirConexion();


            DataTable listAsoc = new DataTable();

            listAsoc = conexion.sqlSelect("SELECT * FROM Lista"); //Databa table con las listas


            for (int i = 0; i < listAsoc.Rows.Count; i++)
            {
                string inicioS = listAsoc.Rows[i][2].ToString();
                string finS = listAsoc.Rows[i][3].ToString();
                DateTime inicio = Convert.ToDateTime(inicioS);
                DateTime fin = Convert.ToDateTime(finS);


                if (Between(DateTime.Today, inicio, fin))
                {
                    idListaVigente = listAsoc.Rows[i][0].ToString();
                    prod = conexion.sqlSelect("SELECT * FROM ListaProductos as LC, Producto as P, Lista as L " +
                        "WHERE LC.producto = P.idProducto AND LC.lista = L.idLista AND L.idLista = " + idListaVigente + "");
                    gridProductos.DataSource = prod;
                    gridProductos.DataBind();
                    conexion.sqlCerrarConexion();
                    return; //Asociada == true, sale del metodo sin redireccionar a la asociacion de listas
                }


            }

        }



        public void mostrarProductos()
        {
            conexion.sqlAbrirConexion();
            prod = conexion.sqlSelect("SELECT * FROM Producto");
            conexion.sqlCerrarConexion();

            for (int i = 0; i < prod.Rows.Count; i++)
            {
                dropDPro.Items.Add(prod.Rows[i][1].ToString());
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            idIngresos = 0;
            verificarPrecios(); //Verifica si tiene la lista vigente asociada
            verLimiteCr();
            agregarDTbl();
            verOrdenesVencidas();
        }

        public void verificarPrecios()
        {
            DataTable cli = new DataTable();
            string query1 = "SELECT clienteNIT FROM Cliente WHERE nombres = '" + DropDownClientes.SelectedItem.Text + "'";


            conexion.sqlAbrirConexion();

            cli = conexion.sqlSelect(query1);
            string nitCliente = cli.Rows[0][0].ToString();
            idCliente = nitCliente;

            string query2 = "SELECT idLista, fechaInicio, fechaFin FROM Lista as L, " +
                "ListaCliente as LC, Cliente as C WHERE C.clienteNIT = LC.cliente " +
                "AND idLista = LC.lista AND clienteNIT = '" + nitCliente + "'"; //Todas las listas asociadas al cliente seleccionado

            DataTable listAsoc = new DataTable();

            listAsoc = conexion.sqlSelect(query2); //Databa table con las listas


            for (int i = 0; i < listAsoc.Rows.Count; i++)
            {
                string inicioS = listAsoc.Rows[i][1].ToString();
                string finS = listAsoc.Rows[i][2].ToString();
                DateTime inicio = Convert.ToDateTime(inicioS);
                DateTime fin = Convert.ToDateTime(finS);


                if (Between(DateTime.Today, inicio, fin))
                {
                    idListaVigente = listAsoc.Rows[i][0].ToString();
                    conexion.sqlCerrarConexion();
                    return; //Asociada == true, sale del metodo sin redireccionar a la asociacion de listas
                }


            }


            MessageBox.Show("El cliente no tiene la lista vigente asociada", "Error en vigencia", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            conexion.sqlCerrarConexion();
            Response.Redirect("/App/Vendedor/ClienteLista.aspx?ID=" + id + "&nombres=" + nombres);

        }

        public static bool Between(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2);
        }

        public void limpiar()
        {
            listado.Clear();
        }

        public void crearDTbl()
        {
            DataTable nueva = new DataTable();
            nueva.Columns.Add("ID ingreso");
            nueva.Columns.Add("idProducto");
            nueva.Columns.Add("Nombre");
            nueva.Columns.Add("Precio");
            nueva.Columns.Add("Cantidad");
            listado = nueva;
        }


        public void agregarDTbl()
        {
            DataTable productos = new DataTable();
            DataTable prod = new DataTable();
            conexion.sqlAbrirConexion();

            productos = conexion.sqlSelect("SELECT P.idProducto, P.nombre, LC.precio " +
                "FROM Producto as P, ListaProductos as LC, Lista as L WHERE" +
                " L.idLista = '" + idListaVigente + "' AND L.idLista = LC.lista AND P.idProducto = LC.producto");

            prod = conexion.sqlSelect("SELECT idProducto FROM Producto WHERE nombre = '" + dropDPro.SelectedItem.Text + "'");

            for (int i = 0; i < productos.Rows.Count; i++)
            {
                string idLista = productos.Rows[i][0].ToString();
                string prodSelected = prod.Rows[0][0].ToString();

                //Verificar que el producto este en la lista vigente
                if (idLista.Equals(prodSelected))
                {
                    string precioS = productos.Rows[i][2].ToString();
                    string cantidadS = txtCantidad.Text;
                    float precio = float.Parse(precioS);
                    float cantidad = float.Parse(cantidadS);

                    total += (precio * cantidad);

                    if (total > limite)
                    {
                        MessageBox.Show("Se esta excediendo en el limite de credito", "Limite credito", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }

                    idIngresos++;

                    object[] o = { idIngresos, idLista, productos.Rows[i][1].ToString(), productos.Rows[i][2].ToString(), txtCantidad.Text };
                    listado.Rows.Add(o);

                    gvProAgre.DataSource = listado;
                    gvProAgre.DataBind();


                    lblTotal.Text = total.ToString();

                    conexion.sqlCerrarConexion();
                    dropDPro.SelectedValue = "-- Seleccione un producto --";
                    txtCantidad.Text = "";

                    return; //Equals == true, el producto existe en dicha lista
                }


            }


            MessageBox.Show("El producto no esta en la lista", "Sin existencia", MessageBoxButton.OK, MessageBoxImage.Exclamation);

        }

        protected void btnGenerarO_Click(object sender, EventArgs e)
        {
            generarOrden();
            txtCantidad.Text = "";
            crearDTbl();
            gvProAgre.DataSource = listado;
            gvProAgre.DataBind();
            dropDPro.SelectedValue = "-- Seleccione un producto --";
            DropDownClientes.SelectedValue = "-- Seleccione un cliente --";
            total = 0;
            lblTotal.Text = "";
            lblLimiteCr.Text = "";
            lblOV.Text = "";
        }

        public void verLimiteCr()
        {
            DataTable credito = new DataTable();
            conexion.sqlAbrirConexion();
            credito = conexion.sqlSelect("SELECT limiteCredito FROM Cliente WHERE clienteNIT = '" + idCliente + "'");
            limite = float.Parse(credito.Rows[0][0].ToString());
            lblLimiteCr.Text = limite.ToString();
            conexion.sqlCerrarConexion();

        }

        public void generarOrden()
        {
            try
            {
                conexion.sqlAbrirConexion();
                string query = "INSERT INTO Orden VALUES ('" + idCliente + "', 'Creada', " + total + ", " + total + ", '" + id + "')";
                conexion.sqlInsert(query);
                idOrden++;

                DataTable cliente = new DataTable();
                cliente = conexion.sqlSelect("SELECT limiteCredito From Cliente WHERE clienteNIT = '" + idCliente + "'");
                float creditoViejo = float.Parse(cliente.Rows[0][0].ToString());
                float creditoNuevo = creditoViejo - total;
                conexion.sqlUpdate("UPDATE Cliente SET limiteCredito = " + creditoNuevo + " WHERE clienteNIT = '" + idCliente + "'");


                generarDetalle();
                conexion.sqlCerrarConexion();
                MessageBox.Show("Orden generada con exito", "Generar orden", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al generar orden", "Generar orden", MessageBoxButton.OK, MessageBoxImage.Exclamation);

            }

        }

        public void generarDetalle()
        {
            try
            {
                for (int i = 0; i < listado.Rows.Count; i++)
                {
                    conexion.sqlAbrirConexion();
                    string idProducto = listado.Rows[i][1].ToString();
                    string cant = listado.Rows[i][4].ToString();
                    string precio = listado.Rows[i][3].ToString();
                    string query = "INSERT INTO DetalleOrden VALUES (" + idOrden + ", " + idProducto +
                        ", " + cant + ", " + precio + ")";
                    conexion.sqlInsert(query);
                    conexion.sqlCerrarConexion();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al generar detalle", "Generar orden", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

        }

        public void obOr()
        {
            DataTable ids = new DataTable();
            conexion.sqlAbrirConexion();
            ids = conexion.sqlSelect("SELECT COUNT(idOrden) as cantidad FROM Orden");
            idOrden = Int32.Parse(ids.Rows[0][0].ToString());
            conexion.sqlCerrarConexion();
        }

        public void verOrdenesVencidas()
        {
            DataTable ordenes = new DataTable();
            string query = "SELECT COUNT(*) as pendiente FROM orden WHERE saldo > 0 and cliente = '" + idCliente + "'";

            conexion.sqlAbrirConexion();
            ordenes = conexion.sqlSelect(query);
            lblOV.Text = ordenes.Rows[0][0].ToString();
            conexion.sqlCerrarConexion();
        }

    }
}
