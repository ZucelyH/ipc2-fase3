﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace Fase3.App.Gerente
{
    public partial class ClienteLista : System.Web.UI.Page
    {
        static string id, nombres;
        Conexion conexion = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();

                mostrarClientes();
                mostrarListaI();
            }
        }

        public void mostrarListas()
        {
            string cliente = DropDownClientes.SelectedItem.Text;

            DataTable asociadas = new DataTable();
            DataTable listas = new DataTable();


            string query1 = "SELECT L.idLista, L.nombre, L.fechaInicio, L.fechaFin FROM Lista AS L, Cliente AS C, ListaCliente AS LC" +
                " WHERE L.idLista = LC.lista AND C.clienteNIT = LC.cliente AND C.nombres = '" + cliente + "'";

            conexion.sqlAbrirConexion();

            try
            {
                asociadas = conexion.sqlSelect(query1);
                dgvAsoc.DataSource = asociadas;
                dgvAsoc.DataBind();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No hay listas asociadas", "Listas asociadas", MessageBoxButton.OK, MessageBoxImage.Information);

            }

            listas = conexion.sqlSelect("SELECT * FROM Lista");
            dgvListas.DataSource = listas;
            dgvListas.DataBind();


            conexion.sqlCerrarConexion();

        }


        public void mostrarClientes()
        {
            DataTable cli = new DataTable();
            conexion.sqlAbrirConexion();
            cli = conexion.sqlSelect("SELECT nombres FROM Cliente");
            conexion.sqlCerrarConexion();

            for (int i = 0; i < cli.Rows.Count; i++)
            {
                DropDownClientes.Items.Add(cli.Rows[i][0].ToString());
            }

        }

        protected void btnVer_Click(object sender, EventArgs e)
        {
            mostrarListas();
        }

        public void mostrarListaI()
        {
            DataTable list = new DataTable();
            conexion.sqlAbrirConexion();
            list = conexion.sqlSelect("SELECT nombre FROM Lista");
            conexion.sqlCerrarConexion();

            for (int i = 0; i < list.Rows.Count; i++)
            {
                dropListas.Items.Add(list.Rows[i][0].ToString());
            }
        }

        protected void btnAsig_Click(object sender, EventArgs e)
        {
            asociar();
        }

        public void asociar()
        {
            if (verificarTraslape())
            {
                MessageBox.Show("Hay traslape", "Lista Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                DataTable cliente = new DataTable();
                DataTable lista = new DataTable();

                conexion.sqlAbrirConexion();
                cliente = conexion.sqlSelect("SELECT clienteNIT FROM Cliente WHERE nombres = '" + DropDownClientes.SelectedItem.Text + "'");
                lista = conexion.sqlSelect("SELECT idLista FROM Lista WHERE nombre = '" + dropListas.SelectedItem.Text + "'");

                try
                {
                    conexion.sqlInsert("INSERT INTO ListaCliente VALUES ('" + cliente.Rows[0][0].ToString() + "', " + lista.Rows[0][0].ToString() + ")");
                    MessageBox.Show("Lista asociada con exito", "Lista Cliente", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lista ya asociada", "Lista Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }


        public static bool Between(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2);
        }

        public bool verificarTraslape()
        {
            DataTable cliente = new DataTable();
            DataTable listasAsoc = new DataTable();
            DataTable listaSelected = new DataTable();

            conexion.sqlAbrirConexion();
            cliente = conexion.sqlSelect("SELECT clienteNIT FROM Cliente WHERE nombres = '" + DropDownClientes.SelectedItem.Text + "'");
            string query = "SELECT L.idLista, L.fechaInicio, L.fechaFin FROM Lista as L, Cliente as C, ListaCliente as LC" +
                " WHERE L.idLista = LC.lista AND C.clienteNIT = LC.cliente AND C.clienteNIT = '" + cliente.Rows[0][0].ToString() + "'";

            listaSelected = conexion.sqlSelect("SELECT idLista, fechaInicio, fechaFin FROM Lista WHERE nombre = '" + dropListas.SelectedItem.Text + "'");

            string inicioSelec = listaSelected.Rows[0][1].ToString();
            string finSelec = listaSelected.Rows[0][2].ToString();
            DateTime inicioDD = Convert.ToDateTime(inicioSelec);
            DateTime finDD = Convert.ToDateTime(finSelec);


            listasAsoc = conexion.sqlSelect(query); //Databa table con las listas


            for (int i = 0; i < listasAsoc.Rows.Count; i++)
            {
                string inicioS = listasAsoc.Rows[i][1].ToString();
                string finS = listasAsoc.Rows[i][2].ToString();
                DateTime inicio = Convert.ToDateTime(inicioS);
                DateTime fin = Convert.ToDateTime(finS);


                if (Between(inicioDD, inicio, fin) || Between(finDD, inicio, fin))
                {
                    MessageBox.Show("La lista seleccionada se traslapa con alguna lista asociada", "Lista Cliente", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    conexion.sqlCerrarConexion();
                    return true; //Traslape == true

                }


            }

            return false;
        }


    }
}