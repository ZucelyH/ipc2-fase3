﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Gerente/Gerente.Master" AutoEventWireup="true" CodeBehind="ClienteLista.aspx.cs" Inherits="Fase3.App.Gerente.ClienteLista" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <h1 style="text-align: center;">Asignar listas</h1><br />

    <div style="margin-left: auto; margin-right: auto; text-align: center;">

        <label for="espec">Seleccione el cliente</label><br />


        <asp:DropDownList  ID="DropDownClientes" runat="server">
            <asp:ListItem Text="-- Seleccione un cliente --"></asp:ListItem>

        </asp:DropDownList>&nbsp;&nbsp;&nbsp;

        <asp:Button type="button" class="btn btn-primary btn-lg" ID="btnVer" runat="server" Text="Ver listas asociadas" OnClick="btnVer_Click" />

        <br />

     </div>

        <asp:Label runat="server" Text="Listas asociadas"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:GridView ID="dgvAsoc" runat="server"></asp:GridView>

    

    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <asp:Label runat="server" Text="Seleccione la lista a asociar"></asp:Label><br />
        <asp:DropDownList id="dropListas" runat="server">
            <asp:ListItem Text="-- Seleccione una lista --"></asp:ListItem>
        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button type="button" class="btn btn-primary btn-lg" ID="btnAsig" runat="server" Text="Asignar" OnClick="btnAsig_Click" />
    </div>
    
    

    


        <asp:Label runat="server" Text="      Listas no asociadas"></asp:Label>


        <asp:GridView ID="dgvListas" runat="server"></asp:GridView>


</asp:Content>
