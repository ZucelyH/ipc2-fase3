﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fase3.App.Gerente
{
    public partial class VerAbonos : System.Web.UI.Page
    {
        static string id, nombres;
        Conexion conexion = new Conexion();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
                verAbonos();
            }
        }

        public void verAbonos()
        {
            DataTable abonos = new DataTable();
            string query = "SELECT A.idAbono, A.orden, A.cantidad, A.fecha, A.metodoPago FROM Abono as A, Orden as O, Empleado as E " +
                "WHERE A.orden = O.idOrden AND O.empleado = E.NITempleado AND E.NITempleado = '" + id + "'";
            conexion.sqlAbrirConexion();
            abonos = conexion.sqlSelect(query);
            gvAbonos.DataSource = abonos;
            gvAbonos.DataBind();
            conexion.sqlCerrarConexion();
        }
    }
}