﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace Fase3.App.Gerente
{
    public partial class aceptarAnularPropias : System.Web.UI.Page
    {
        static string id, nombres;
        Conexion conexion = new Conexion();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
                txtMontoAnulacion.Enabled = false;
                mostrarOrdenes();
            }
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            try
            {
                string idOrden = dropIds.SelectedItem.Text;


                DataTable estado = new DataTable();
                string queryEstado = "SELECT estado FROM Orden WHERE idOrden = " + idOrden;
                conexion.sqlAbrirConexion();
                estado = conexion.sqlSelect(queryEstado);
                string estadoS = estado.Rows[0][0].ToString();


                if (estadoS.Equals("Anulada")) //SI LA ORDEN SELECCIONADA ESTA ANULADA
                {
                    conexion.sqlCerrarConexion();
                    dropIds.Items.Clear();
                    dropIds.Items.Add("-- Seleccione un id --");
                    dropIds.SelectedValue = "-- Seleccione un id --";
                    MessageBox.Show("No se puede cambiar el estado de una orden anulada", "Orden anulada", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else if (estadoS.Equals("Pagada"))  //SI LA ORDEN SELECCIONADA ESTA PAGADAS
                {
                    conexion.sqlCerrarConexion();
                    dropIds.Items.Clear();
                    dropIds.Items.Add("-- Seleccione un id --");
                    dropIds.SelectedValue = "-- Seleccione un id --";
                    MessageBox.Show("No se puede cambiar el estado de una orden pagada", "Orden pagada", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else //SI LA ORDEN SELECCIONADA ESTA CREADA O ACEPTADA
                {
                    string estadoSeleccionado = dropEstados.SelectedItem.Text;


                    if (estadoSeleccionado.Equals("Aceptada"))
                    {
                        string query = "UPDATE Orden SET estado = '" + estadoSeleccionado + "' WHERE idOrden = " + idOrden;
                        conexion.sqlAbrirConexion();
                        conexion.sqlUpdate(query);
                        conexion.sqlCerrarConexion();
                        dropIds.Items.Clear();
                        dropIds.Items.Add("-- Seleccione un id --");
                        dropIds.SelectedValue = "-- Seleccione un id --";
                        txtMontoAnulacion.Enabled = false;
                        mostrarOrdenes();
                        MessageBox.Show("Estado cambiado", "Cambio de estado", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        DataTable orden = new DataTable();
                        string queryOrden = "SELECT montoTotal, saldo FROM Orden WHERE idOrden = " + idOrden;
                        conexion.sqlAbrirConexion();
                        orden = conexion.sqlSelect(queryOrden);
                        float montoFactura = float.Parse(orden.Rows[0][0].ToString()) - float.Parse(orden.Rows[0][1].ToString());
                        string factura = "INSERT INTO Factura VALUES ('" + DateTime.Today.ToString() + "', " + montoFactura + ", " + idOrden + ")";
                        conexion.sqlInsert(factura);
                        string nota = "INSERT INTO NotaDeCredito VALUES (" + idOrden + ", '" + DateTime.Today.ToString() + "', " + txtMontoAnulacion.Text + ")";
                        conexion.sqlInsert(nota);
                        string query = "UPDATE Orden SET estado = '" + estadoSeleccionado + "' WHERE idOrden = " + idOrden;
                        conexion.sqlUpdate(query);
                        conexion.sqlCerrarConexion();
                        dropIds.Items.Clear();
                        dropIds.Items.Add("-- Seleccione un id --");
                        dropIds.SelectedValue = "-- Seleccione un id --";
                        txtMontoAnulacion.Enabled = false;
                        mostrarOrdenes();
                        MessageBox.Show("Estado cambiado", "Cambio de estado", MessageBoxButton.OK, MessageBoxImage.Information);

                    }


                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo salio mal", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }


        }
        public void mostrarOrdenes()
        {
            DataTable ordenes = new DataTable();
            conexion.sqlAbrirConexion();
            string query = "SELECT * FROM Orden WHERE empleado = '" + id + "'";

            try
            {
                ordenes = conexion.sqlSelect(query);
                gvOrdenes.DataSource = ordenes;
                gvOrdenes.DataBind();
                dropIds.Items.Clear();
                dropIds.Items.Add("-- Seleccione un id --");
                dropIds.SelectedValue = "-- Seleccione un id --";

                for (int i = 0; i < ordenes.Rows.Count; i++)
                {
                    dropIds.Items.Add(ordenes.Rows[i][0].ToString());
                }

                conexion.sqlCerrarConexion();

            }
            catch (Exception ex)
            {
                MessageBox.Show("No tiene empleados a su cargo", "Sin empleados", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                conexion.sqlCerrarConexion();
            }
        }

        protected void dropEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            string seleccionado = dropEstados.SelectedItem.Text;
            if (seleccionado.Equals("Anulada"))
            {
                txtMontoAnulacion.Enabled = true;
            }
            else if (seleccionado.Equals("Aceptada"))
            {
                txtMontoAnulacion.Enabled = false;
            }

        }
    }
}