﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Gerente/Gerente.Master" AutoEventWireup="true" CodeBehind="Abonos.aspx.cs" Inherits="Fase3.App.Gerente.Abonos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    
     <br />
    <asp:GridView ID="gvOrdenes" runat="server"></asp:GridView>
    <br /><br />

     <div style="margin-left: auto; margin-right: auto; text-align: center;">
            <label for="espec">Seleccione el id de la orden</label><br />
            <asp:DropDownList ID="dropIds" runat="server">
                <asp:ListItem Text="-- Seleccione un id --"></asp:ListItem>
            </asp:DropDownList>
        </div><br /><br />

        <div style="margin-left: auto; margin-right: auto; text-align: left;">
            <label for="espec">Escriba la cantidad a abonar</label><br />
            <asp:TextBox ID="txtCantidad" runat="server"></asp:TextBox><br /><Br />

            <label for="espec">Seleccione el tipo de moneda</label><br />
            <asp:DropDownList AutoPostBack="true" ID="dropMoneda" runat="server" OnSelectedIndexChanged="dropMoneda_SelectedIndexChanged">
                <asp:ListItem Text="-- Seleccione una moneda --"></asp:ListItem>
            </asp:DropDownList><br /><br /><br />

            <label for="espec">Seleccione el metodo de pago</label><br />
            <asp:DropDownList AutoPostBack="true" ID="dropTipoP" runat="server" OnSelectedIndexChanged="dropTipoP_SelectedIndexChanged">
                <asp:ListItem Text="-- Seleccione un tipo de pago --"></asp:ListItem>
                <asp:ListItem Text="Cheque"></asp:ListItem>
                <asp:ListItem Text="Efectivo"></asp:ListItem>
                <asp:ListItem Text="Tarjeta"></asp:ListItem>
            </asp:DropDownList>
        </div>

        <div style="margin-left: auto; margin-right: auto; text-align: center;">
            <asp:Label ID="Label1" runat="server" Text="Usted va a abonar (en $):  "></asp:Label>
            <asp:Label ID="lblCantidad" runat="server" Text=""></asp:Label> <br /><br />
            <asp:Label ID="Label2" runat="server" Text="Información tarjeta: "></asp:Label><br /><br />
            <asp:TextBox ID="txtEmisor" runat="server"></asp:TextBox><br /><br />
            <asp:TextBox ID="txtNoA" runat="server"></asp:TextBox><br /><br />

            <div style="margin-left: auto; margin-right: auto; text-align: right;">
                <asp:Button  type="button" class="btn btn-info btn-lg" ID="Button1" runat="server" Text="Hacer abono" OnClick="Button1_Click" />
            </div>

            <asp:Label ID="Label3" runat="server" Text="Información cheque: "></asp:Label><br /><br />
            <asp:TextBox ID="txtBanco" runat="server"></asp:TextBox><br /><br />
            <asp:TextBox ID="txtcuenta" runat="server"></asp:TextBox><br /><br />
            <asp:TextBox ID="txtCheque" runat="server"></asp:TextBox><br /><br />
        </div>


</asp:Content>
