﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Gerente/Gerente.Master" AutoEventWireup="true" CodeBehind="AcAnOempleados.aspx.cs" Inherits="Fase3.App.Gerente.AcAnOempleados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <br /><br />
    <asp:GridView ID="gvEmpleado" runat="server"></asp:GridView>
    <br /><br />

    <div style="margin-left: auto; margin-right: auto; text-align: center;">

        <label for="espec">Seleccione un empleado</label><br />


        <asp:DropDownList  ID="dropEmpleados" runat="server">
            <asp:ListItem Text="-- Seleccione un empleado --"></asp:ListItem>
        </asp:DropDownList><br /><br />
        <asp:Button type="button" class="btn btn-danger" ID="btnVerOrd" runat="server" Text="Ver ordenes" OnClick="btnVerOrd_Click1"  />
        <br /><br />

        <asp:GridView ID="gvOrdenes" runat="server"></asp:GridView>
     </div> <br />

        <div style="margin-left: auto; margin-right: auto; text-align: left;">
            <label for="espec">Seleccione id de la orden</label><br />
            <asp:DropDownList ID="dropIds" runat="server" OnSelectedIndexChanged="dropIds_SelectedIndexChanged">
                <asp:ListItem Text="-- Seleccione un id --"></asp:ListItem>
            </asp:DropDownList>
        </div>

        <div style="margin-left: auto; margin-right: auto; text-align: center;">
            <label for="espec">Seleccione el estado</label><br />
            <asp:DropDownList AutoPostBack="true" ID="dropEstados" runat="server" OnSelectedIndexChanged="dropEstados_SelectedIndexChanged">
                <asp:ListItem Text="Aceptada"></asp:ListItem>
                <asp:ListItem Text="Anulada"></asp:ListItem>
            </asp:DropDownList><br /><Br />
            <asp:Label ID="Label1" runat="server" Text="Escriba el monto para anulacion: "></asp:Label>
            <asp:TextBox ID="txtMontoAnulacion" runat="server"></asp:TextBox>
            <br /><Br />
            <asp:Button type="button" class="btn btn-danger" ID="Button2" runat="server" Text="Cambiar estado" OnClick="btnCambiar_Click" />
        </div>




</asp:Content>
