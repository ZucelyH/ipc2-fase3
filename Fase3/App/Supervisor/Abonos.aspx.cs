﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace Fase3.App.Supervisor
{
    public partial class Abonos : System.Web.UI.Page
    {
        static string id, nombres;
        Conexion conexion = new Conexion();




        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
                mostrarOrdenes();
                mostrarMonedas();
                obtenerIdAbono();
                txtBanco.Enabled = false;
                txtcuenta.Enabled = false;
                txtCheque.Enabled = false;
                txtEmisor.Enabled = false;
                txtNoA.Enabled = false;
            }
        }

        protected void dropTipoP_SelectedIndexChanged(object sender, EventArgs e)
        {
            string seleccionado = dropTipoP.SelectedItem.Text;
            if (seleccionado.Equals("Tarjeta"))
            {
                txtEmisor.Enabled = true;
                txtNoA.Enabled = true;
                txtBanco.Enabled = false;
                txtcuenta.Enabled = false;
                txtCheque.Enabled = false;
            }
            else if (seleccionado.Equals("Cheque"))
            {
                txtEmisor.Enabled = false;
                txtNoA.Enabled = false;
                txtBanco.Enabled = true;
                txtcuenta.Enabled = true;
                txtCheque.Enabled = true;
            }
            else
            {
                txtBanco.Enabled = false;
                txtcuenta.Enabled = false;
                txtCheque.Enabled = false;
                txtEmisor.Enabled = false;
                txtNoA.Enabled = false;
            }
        }

        public void mostrarOrdenes()
        {
            DataTable ordenes = new DataTable();
            conexion.sqlAbrirConexion();
            string query = "SELECT * FROM Orden WHERE empleado = '" + id + "' AND  estado = 'Aceptada'";

            try
            {
                ordenes = conexion.sqlSelect(query);
                gvOrdenes.DataSource = ordenes;
                gvOrdenes.DataBind();

                dropIds.Items.Clear();
                dropIds.Items.Add("-- Seleccione un id --");
                for (int i = 0; i < ordenes.Rows.Count; i++)
                {
                    dropIds.Items.Add(ordenes.Rows[i][0].ToString());
                }


                conexion.sqlCerrarConexion();

            }
            catch (Exception ex)
            {
                MessageBox.Show("No tiene monedas asociadas", "Sin monedas", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                conexion.sqlCerrarConexion();
            }
        }

        protected void dropMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable moneda = new DataTable();
            conexion.sqlAbrirConexion();
            string query = "SELECT * FROM Moneda WHERE nombre = '" + dropMoneda.SelectedItem.Text + "'";

            try
            {
                moneda = conexion.sqlSelect(query);
                float cantidad = float.Parse(txtCantidad.Text);
                float tasa = float.Parse(moneda.Rows[0][2].ToString());
                float conversion = cantidad * tasa;
                lblCantidad.Text = conversion.ToString();
                conexion.sqlCerrarConexion();

            }
            catch (Exception ex)
            {
                MessageBox.Show("No escribio una cantidad", "Cantidad", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                conexion.sqlCerrarConexion();
            }
        }

        public void mostrarMonedas()
        {
            DataTable monedas = new DataTable();
            conexion.sqlAbrirConexion();
            string query = "SELECT * FROM Moneda";

            try
            {
                monedas = conexion.sqlSelect(query);

                for (int i = 0; i < monedas.Rows.Count; i++)
                {
                    string item = monedas.Rows[i][0].ToString();
                    dropMoneda.Items.Add(item);
                }


                conexion.sqlCerrarConexion();

            }
            catch (Exception ex)
            {
                MessageBox.Show("No tiene monedas asociadas", "Sin monedas", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                conexion.sqlCerrarConexion();
            }
        }

        public void realizarAbono()
        {

            DataTable orden = new DataTable();
            conexion.sqlAbrirConexion();
            string queryOrden = "SELECT * FROM Orden WHERE idOrden = " + dropIds.SelectedItem.Text + "";
            orden = conexion.sqlSelect(queryOrden);
            float saldoPendiente = float.Parse(orden.Rows[0][4].ToString());
            float abono = float.Parse(lblCantidad.Text);
            float monto = float.Parse(orden.Rows[0][3].ToString());

            if (abono > saldoPendiente)
            {
                MessageBox.Show("El abono excede el saldo pendiente", "Excediente", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                conexion.sqlCerrarConexion();
                limpiar();
                return;
            }
            else if (abono == saldoPendiente)
            {
                string hoy = DateTime.Today.ToString();
                string query = "INSERT INTO Abono VALUES (" + dropIds.SelectedItem.Text + ", " + lblCantidad.Text +
                    ", '" + hoy + "', '" + dropTipoP.SelectedItem.Text + "', '" + dropMoneda.SelectedItem.Text + "')";
                conexion.sqlInsert(query);

                int idLastAbono = obtenerIdAbono();
                float nuevoSaldo = saldoPendiente - abono;
                string upEstado = "UPDATE Orden SET estado = 'Pagada', saldo = " + nuevoSaldo + " WHERE idOrden = " + dropIds.SelectedItem.Text + "";
                conexion.sqlAbrirConexion();
                conexion.sqlUpdate(upEstado);
                string queryFactura = "INSERT INTO Factura VALUES ('" + DateTime.Today.ToString() + "', " + monto + ", " + dropIds.SelectedItem.Text + ")";
                conexion.sqlInsert(queryFactura);

                MessageBox.Show("Abono realizado con exito. El saldo pendiente es 0", "Factura generada", MessageBoxButton.OK, MessageBoxImage.Information);
                conexion.sqlCerrarConexion();
                limpiar();
            }
            else
            {
                string hoy = DateTime.Today.ToString();
                string query = "INSERT INTO Abono VALUES (" + dropIds.SelectedItem.Text + ", " + lblCantidad.Text +
                    ", '" + hoy + "', '" + dropTipoP.SelectedItem.Text + "', '" + dropMoneda.SelectedItem.Text + "')";
                conexion.sqlInsert(query);
                float nuevoSaldo = saldoPendiente - abono;
                string queryUp = "UPDATE Orden SET saldo = " + nuevoSaldo + " WHERE idOrden = " + dropIds.SelectedItem.Text + "";
                conexion.sqlUpdate(queryUp);
                MessageBox.Show("Abono realizado con exito", "Abono realizado", MessageBoxButton.OK, MessageBoxImage.Information);
                conexion.sqlCerrarConexion();
                limpiar();
            }



        }

        public int obtenerIdAbono()
        {
            DataTable ids = new DataTable();
            conexion.sqlAbrirConexion();
            ids = conexion.sqlSelect("SELECT COUNT(idAbono) as cantidad FROM Abono");
            int idUltimoAbono;
            idUltimoAbono = Int16.Parse(ids.Rows[0][0].ToString());
            conexion.sqlCerrarConexion();
            return idUltimoAbono;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            realizarAbono();
        }

        public void limpiar()
        {

            mostrarOrdenes();



            dropIds.SelectedValue = "-- Seleccione un id --";
            txtCantidad.Text = "";
            dropMoneda.SelectedValue = "-- Seleccione una moneda --";
            dropTipoP.SelectedValue = "-- Seleccione un tipo de pago --";
            txtBanco.Text = "";
            txtcuenta.Text = "";
            txtCheque.Text = "";
            txtEmisor.Text = "";
            txtNoA.Text = "";
            txtBanco.Enabled = false;
            txtcuenta.Enabled = false;
            txtCheque.Enabled = false;
            txtEmisor.Enabled = false;
            txtNoA.Enabled = false;
            lblCantidad.Text = "";
            obtenerIdAbono();

        }
    }
}