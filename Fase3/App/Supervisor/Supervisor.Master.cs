﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fase3.App.Supervisor
{
    public partial class Supervisor : System.Web.UI.MasterPage
    {
        static string id, nombres;
        int contador = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();

            }

            Perfil perfil = new Perfil();
            int datos = perfil.getDatos();

            if (datos != contador)  //Hubo cambios en el perfil
            {
                IDfromPerfil();
                contador = datos;
            }
            else
            {
                //No hubo cambios
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Login/Login.aspx");
        }

        protected void btnPerfil_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/Perfil.aspx?ID=" + id + "&nombres=" + nombres);
        }

        public void IDfromPerfil()
        {
            Perfil perfil = new Perfil();
            string nuevoId = perfil.getID();
            string nuevoNom = perfil.getNombres();

            if (nuevoId != id)
            {
                id = nuevoId;
                nombres = nuevoNom;

            }
        }
        protected void btnAddCli_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/FormularioCliente.aspx?ID=" + id + "&nombres=" + nombres);
        }

        protected void btnAsoc_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/ClienteLista.aspx?ID=" + id + "&nombres=" + nombres);
        }

        protected void btnGO_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/GenerarOrden.aspx?ID=" + id + "&nombres=" + nombres);
        }

        protected void btnCGO_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/OrdenesGeneradas.aspx?ID=" + id + "&nombres=" + nombres);
        }

        protected void btnAAem_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/AcAnOempleados.aspx?ID=" + id + "&nombres=" + nombres);
        }

        protected void btnAbonar_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/Abonos.aspx?ID=" + id + "&nombres=" + nombres);
        }

        protected void btnVerA_Click(object sender, EventArgs e)
        {
            Response.Redirect("/App/Supervisor/VerAbonos.aspx?ID=" + id + "&nombres=" + nombres);
        }


    }
}