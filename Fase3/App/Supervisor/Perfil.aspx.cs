﻿using Fase3.App.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace Fase3.App.Supervisor
{
    public partial class Perfil : System.Web.UI.Page
    {
        Conexion conexion = new Conexion();
        static string id, nombres;
        static int datos = 0;

        public int getDatos()
        {
            return datos;
        }

        public string getID()
        {
            return id;
        }

        public string getNombres()
        {
            return nombres;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] == null) return;
                if (Request.QueryString["nombres"] == null) return;

                id = Request.QueryString["ID"].ToString();
                nombres = Request.QueryString["nombres"].ToString();
                mostrarDatos();
            }

        }

        protected void btnNIT_Click(object sender, EventArgs e)
        {

        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            string query = "UPDATE  Empleado SET NITempleado='" + txtNIT.Text.Trim() + "', nombres='" + txtNombres.Text.Trim() +
                "', apellidos='" + txtApellidos.Text.Trim() + "', fechaNacimiento='" + txtNacimiento.Text.Trim() +
                "', direccion='" + txtDireccion.Text.Trim() + "', telefono='" + txtTelefono.Text.Trim() + "', celular='" + txtCelular.Text.Trim() +
                "', email='" + txtMail.Text.Trim() + "', passwo='" + txtPassword.Text.Trim() + "' WHERE NITempleado = '" + id + "'";

            conexion.sqlAbrirConexion();
            conexion.sqlUpdate(query);
            conexion.sqlCerrarConexion();

            id = txtNIT.Text;
            nombres = txtNombres.Text;

            datos++;
            MessageBox.Show("Cambios realizados", "Modificacion de perfil", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void mostrarDatos()
        {
            DataTable dtd = new DataTable();



            conexion.sqlAbrirConexion();
            dtd = conexion.sqlSelect("SELECT * FROM Empleado WHERE NITempleado = '" + id + "'");
            conexion.sqlCerrarConexion();

            txtNIT.Text = dtd.Rows[0][0].ToString();
            txtNombres.Text = dtd.Rows[0][1].ToString();
            txtApellidos.Text = dtd.Rows[0][2].ToString();
            txtNacimiento.Text = dtd.Rows[0][3].ToString();
            txtDireccion.Text = dtd.Rows[0][4].ToString();
            txtTelefono.Text = dtd.Rows[0][5].ToString();
            txtCelular.Text = dtd.Rows[0][6].ToString();
            txtMail.Text = dtd.Rows[0][7].ToString();
            txtPassword.Text = dtd.Rows[0][10].ToString();


        }
    }
}